class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set personName(anyName) {
        this._name = anyName;
    }

    get personName() {
        return this._name;
    }

    set personAge(anyAge) {
        this._age = anyAge;
    }

    get personAge() {
        return this._age;
    }

    set personSalary(anySalary) {
        this.salary = anySalary;
    }

    get personSalary() {
        return this.salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, language) {
        super(name, age, salary);
        this.language = language;
    }
    set salary(anySalary) {
        this._salary = anySalary * 3;
    }

    get salary() {
        return this._salary;
    }
}

let progr1 = new Programmer('Neo', 36, 1000, ['C++', 'Java']);
console.log(progr1);

let progr2 = new Programmer('Trinity', 32, 1500, 'Java Script');
console.log(progr2);


